\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key e \major

		R2*4  |
%% 5
		e' 4 b  |
		fis' 4 b  |
		gis' 8 ( fis' e' ) gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
%% 10
		b 4 e' 8 e'  |
		fis' 8 ( gis' ) fis' ( e' )  |
		e' 4 ( dis' )  |
		r8 b' 16 b' b' 8 b'  |
		r8 b' 16 b' b' 8 b'  |
%% 15
		r8 gis' 16 gis' gis' 8 gis'  |
		r8 gis' 16 gis' gis' 8 gis'  |
		r8 a' 16 a' a' 8 a'  |
		r8 gis' 16 gis' gis' 8 gis'  |
		a' 8 ( b' ) a' r  |
%% 20
		e' 4 b  |
		fis' 4 b  |
		gis' 8 fis' e' gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
%% 25
		b 4 e'  |
		fis' 8 gis' fis' e'  |
		e' 4 ( dis' )  |
		e' 4 b  |
		fis' 4 b  |
%% 30
		gis' 8 fis' e' gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
		b 4 e'  |
		fis' 8 gis' fis' e'  |
%% 35
		e' 2 ~  |
		e' 2  |
		gis' 8 ( fis' e' ) gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
%% 40
		b 4 e' 8 e'  |
		fis' 8 ( gis' ) fis' ( e' )  |
		e' 4 ( dis' )  |
		r8 b' 16 b' b' 8 b'  |
		r8 b' 16 b' b' 8 b'  |
%% 45
		r8 gis' 16 gis' gis' 8 gis'  |
		r8 gis' 16 gis' gis' 8 gis'  |
		r8 a' 16 a' a' 8 a'  |
		r8 gis' 16 gis' gis' 8 gis'  |
		a' 8 ( b' ) a' ( gis' )  |
%% 50
		gis' 2 ~  |
		gis' 2  |
		\bar "|."
	}

	\new Lyrics\lyricsto "soprano" {
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __

		¡A -- le -- lu __ ya! ¡A -- le -- lu __ ya!
		¡A -- le -- lu __ ya! ¡A -- le -- lu __ ya!
		¡A -- le -- lu __ ya! ¡A -- le -- lu __ "ya! ¡A" --
		le __ "lu..."

		El Se -- ñor Dios to -- do -- po -- de -- ro -- so
		ha ve -- ni -- "do a" dar -- nos sal -- va -- ción. __
		U -- "na es" -- tre -- lla i -- lu -- mi -- "na el" pue -- blo
		don -- de na -- ce nues -- tro re -- den -- tor. __

		%A -- le -- lu -- ya,
		                     A __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __

		¡A -- le -- lu __ ya! ¡A -- le -- lu __ ya!
		¡A -- le -- lu __ ya! ¡A -- le -- lu __ ya!
		¡A -- le -- lu __ ya! ¡A -- le -- lu __ "ya! ¡A" --
		le __ lu __ ya! __
	}

>>
