\context ChordNames
	\chords {
		\set Score.markFormatter = #format-mark-box-numbers

		% intro
		\skip 2*4

		% aleluya, aleluya...
		\mark \default
		\skip 2*8
		\skip 2*7

		% el sennor dios...
		\mark \default
		\skip 2*8
		\skip 2*7

		% aleluya, aleluya...
		\mark \default
		\skip 2*8
		\skip 2*8
	}
