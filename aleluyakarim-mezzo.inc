\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key e \major

		R2*4  |
%% 5
		e' 4 b  |
		fis' 4 b  |
		gis' 8 ( fis' e' ) gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
%% 10
		b 4 e' 8 e'  |
		fis' 8 ( gis' ) fis' ( e' )  |
		e' 4 ( dis' )  |
		e' 4 b  |
		fis' 4 b  |
%% 15
		gis' 8 ( fis' e' ) gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
		b 4 e' 8 e'  |
		fis' 8 ( gis' ) fis' r  |
%% 20
		e' 4 b  |
		fis' 4 b  |
		gis' 8 fis' e' gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
%% 25
		b 4 e'  |
		fis' 8 gis' fis' e'  |
		e' 4 ( dis' )  |
		e' 4 b  |
		fis' 4 b  |
%% 30
		gis' 8 fis' e' gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
		b 4 e'  |
		R2  |
%% 35
		e' 4 b  |
		fis' 4 b  |
		gis' 8 ( fis' e' ) gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
%% 40
		b 4 e' 8 e'  |
		fis' 8 ( gis' ) fis' ( e' )  |
		e' 4 ( dis' )  |
		e' 4 b  |
		fis' 4 b  |
%% 45
		gis' 8 ( fis' e' ) gis'  |
		fis' 4 dis'  |
		e' 4 cis'  |
		b 4 e' 8 e'  |
		fis' 8 ( gis' ) fis' ( e' )  |
%% 50
		e' 2 ~  |
		e' 2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "mezzo" {
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ "lu..."

		El Se -- ñor Dios to -- do -- po -- de -- ro -- so
		ha ve -- ni -- "do a" dar -- nos sal -- va -- ción. __
		U -- "na es" -- tre -- lla i -- lu -- mi -- "na el" pue -- blo
		don -- de na -- "ce..."

		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __
	}

>>
