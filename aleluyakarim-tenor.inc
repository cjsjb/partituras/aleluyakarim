\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble_8"
		\key e \major

		R2*4  |
%% 5
		e 4 b,  |
		fis 4 b,  |
		gis 8 ( fis e ) gis  |
		fis 4 dis  |
		e 4 cis  |
%% 10
		b, 4 e 8 e  |
		fis 8 ( gis ) fis ( e )  |
		e 4 ( dis )  |
		b 4 b  |
		b 4 b  |
%% 15
		cis' 8 ( dis' e' ) cis'  |
		cis' 4 bis  |
		cis' 4 a  |
		gis 4 b 8 b  |
		cis' 8 ( dis' ) cis' ( b )  |
%% 20
		b 2 ~  |
		b 2  |
		gis 8 fis e gis  |
		fis 4 dis  |
		e 4 cis  |
%% 25
		b, 4 e  |
		fis 8 gis fis e  |
		e 4 ( dis )  |
		b 4 b  |
		b 4 b  |
%% 30
		cis' 8 dis' e' cis'  |
		cis' 4 bis  |
		cis' 4 a  |
		gis 4 b  |
		cis' 8 dis' cis' b  |
%% 35
		b 2 ~  |
		b 2  |
		gis 8 ( fis e ) gis  |
		fis 4 dis  |
		e 4 cis  |
%% 40
		b, 4 e 8 e  |
		fis 8 ( gis ) fis ( e )  |
		e 4 ( dis )  |
		b 4 b  |
		b 4 b  |
%% 45
		cis' 8 ( dis' e' ) cis'  |
		cis' 4 bis  |
		cis' 4 a  |
		gis 4 b 8 b  |
		cis' 8 ( dis' ) cis' ( b )  |
%% 50
		b 2 ~  |
		b 2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "tenor" {
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __

		%El Se -- ñor Dios
		                   "...to" -- do -- po -- de -- ro -- so
		ha ve -- ni -- "do a" dar -- nos sal -- va -- ción. __
		U -- "na es" -- tre -- lla i -- lu -- mi -- "na el" pue -- blo
		don -- de na -- ce nues -- tro re -- den -- tor. __

		%A -- le -- lu -- ya,
		                      A __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __
		A -- le -- lu -- ya, a __ le -- lu -- ya,
		a -- le -- lu -- ya, a -- le __ lu __ ya. __
	}

>>
